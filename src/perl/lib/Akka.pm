package Akka;

use strict;
use akka_perl_mod;

use vars qw($ISO06 $CP1256 $ISIRI3342);

$ISO06=3;
$CP1256=6;
$ISIRI3342=7;

sub load_glyph_file {
  my $cnum = shift @_;
  my $filename = shift @_;
  open(FILTER, "$filename");
  my @filecontent =  <FILTER>;
  close(FILTER);
  @filecontent = grep(!/^\s*$/, @filecontent);
  @filecontent = grep(!/^\s*\#/, @filecontent);
  my $glyphline;
  foreach $glyphline (@filecontent) {
    my @gs = split (/ +/, $glyphline);
    akka_perl_mod::load_glyph_map($cnum, $gs[0], $gs[1], $gs[2], $gs[3], $gs[4])
;
  };
};

sub set_direction {
  akka_perl_mod::set_direction(@_);
};

sub set_bidi_approximation {
  akka_perl_mod::set_bidiapprox(@_);
};

sub remove_console {
  akka_perl_mod::remove_console(@_);
}

sub add_console {
  akka_perl_mod::add_console(@_);
}

sub process_glyph {
  akka_perl_mod::process_glyph(@_);
}

sub set_keymap {
  akka_perl_mod::set_keymap(@_);
}

sub set_default_keymap {
  akka_perl_mod::set_default_keymap(@_);
}

sub terminate_server {
  akka_perl_mod::terminate_server(@_);
}

return 1;
