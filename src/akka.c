/*
 * acon is a utility to facilitate the right to left writing
 * Copyright 1999 Ahmed Abdel-Hamid Mohamed <ahmedam@mail.usa.com>
 *
 * Akka is more general purpose console server originally based on acon
 * Chahine Hamila (hchahine@magic.fr) 2000, 2001.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 *
 */


#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>

#ifdef __linux__
  #include <linux/vt.h>
  #include <linux/kd.h>
#endif

#ifdef __FreeBSD__
  #include <sys/consio.h>
#endif

#include "akkaperms.h"
#include "child.h"
#include "akka-server.h"

int main(int argc,char **argv)
{
  int i,tty;
  
  get_ids();
  seteuid(user_id);
  
  /* get next available console
     Refer to the Akka hackers guide to see the idea behind this...
  */
  seteuid(akka_id);
  if((i=open("/dev/console",O_RDONLY))==-1)
    {
      perror("Cant open /dev/console");
      return 1;
    }
  seteuid(user_id);

#if defined(__FreeBSD__) || defined(__linux)
  if(ioctl(i,VT_OPENQRY,&tty))          /* Linux/FreeBSD: get next available console number */
    {
      perror("Error in akka.c: VT_OPENQRY");
      return 1;
    }
  close(i);
#endif

  if(tty<8)
    tty=8;
  
  /* WARNING : THIS IS SUPPOSED TO BE EXTREMELY UGLY, mixing sigs and threads!!!!
     ONE KNOWN CASE OF TERMINAL CORRUPTION CAUSED BY THIS!!! (fixed after rebooting, not 
     renewed) */
  signal(SIGINT, exit);
  signal(SIGTERM, exit);
  
  printf("Akka is loaded\n");

  start_corba_akka_server(argc, argv);
  
  runchild(tty);
  return 0;
}

