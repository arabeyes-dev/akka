/*****************************************************************************************/
/* ctools_update for sh                                                                  */
/*****************************************************************************************/

#include "orb/orbit.h"
#include "akka.h"
#include <stdio.h>

Akka akka_client;


int main(int argc, char *argv[]) {

  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE * ifp;
  char * ior;
  char filebuffer[1024];

  CORBA_exception_init(&ev);
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    printf("we got exception %d from ctoolsUpdate!\n", ev._major);
    return 1;
  }

  ifp = fopen("/tmp/.akka.ior","r");
  if( ifp == NULL ) {
    g_error("No .akka.ior file!");
    exit(-1);
  }

  fgets(filebuffer,1024,ifp);
  ior = g_strdup(filebuffer);

  fclose(ifp);
 
  akka_client = CORBA_ORB_string_to_object(orb, ior, &ev);

  if (!akka_client) {
    printf("Cannot bind to %s\n", ior);
    if(ev._major != CORBA_NO_EXCEPTION)
      printf("we got exception %d from ctoolsUpdate!\n", ev._major);
    return 1;
  }
  
 
  Akka_ctoolsUpdate(akka_client, &ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    printf("we got exception %d from ctoolsUpdate!\n", ev._major);
    return 1;
  }
  
  /* Clean up */
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);
  
  return 0;
}
