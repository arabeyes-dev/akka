/*
 * This file is to sync this utility with the usual console tools
 * Akka Server - Chahine Hamila (hchahine@magic.fr) Sept 2000
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 *
 */

/* Loading fonts, keymaps, and translation maps is not the responsibility
   of this program anymore.
   You can automate loading special fonts through scripts now using the usual 
   console tools.
*/

void copy_console_settings(int from_tty, int to_tty, int from_cons, int to_cons);
