/* C Akka client implementation for SWIG wrappers 
 * Chahine Hamila & Chokri Mraidha - 2000,2001
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "orb/orbit.h"
#include "akka.h"



Akka akka_client;

void set_direction(int cnum, int dir) {
  int argc = 1;
  char *argv[]  = {""};

  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE *ifp;
  char *ior;
  char filebuffer[1024];
  
  CORBA_exception_init(&ev);
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  ifp = fopen("/tmp/.akka.ior","r");
  if(ifp==NULL){
    perror("No .akka.ior file");
    exit(EXIT_FAILURE);
  }

  fgets(filebuffer,1024,ifp);
  ior = strdup(filebuffer);

  fclose(ifp);
 
  akka_client = CORBA_ORB_string_to_object(orb, ior, &ev);
  if (!akka_client) {
    fprintf(stderr, "Cannot bind to %s\n", ior);
    return;
  }

  Akka_setDirection(akka_client,cnum,dir,&ev);

  if(ev._major!=CORBA_NO_EXCEPTION){
    fprintf(stderr, "Exception %d in setdirection!\n", ev._major);
    return;
  }
  
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);  
}

void set_bidiapprox(int cnum, int bda, int charset) {
  int argc = 1;
  char *argv[]  = {""};

  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE *ifp;
  char *ior;
  char filebuffer[1024];
  
  CORBA_exception_init(&ev);
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  ifp=fopen("/tmp/.akka.ior","r");
  if(ifp==NULL){
    perror("No .akka.ior file");
    exit(EXIT_FAILURE);
  }

  fgets(filebuffer,1024,ifp);
  ior = strdup(filebuffer);

  fclose(ifp);
 
  akka_client=CORBA_ORB_string_to_object(orb, ior, &ev);
  if(!(akka_client)){
    fprintf(stderr, "Cannot bind to %s\n", ior);
    return;
  }


  Akka_setBidiApproximation(akka_client,cnum,bda,charset, &ev);

  if(ev._major!=CORBA_NO_EXCEPTION) {
    fprintf(stderr, "Exception %d in setBidiApproximation!\n", ev._major);
    return;
  }
  
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);  
}

void remove_console(int cnum) {
  int argc = 1;
  char *argv[] = {""};

  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE * ifp;
  char * ior;
  char filebuffer[1024];
  
  CORBA_exception_init(&ev);
  orb=CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  ifp = fopen("/tmp/.akka.ior","r");
  if( ifp == NULL ) {
    perror("No .akka.ior file");
    exit(EXIT_FAILURE);
  }

  fgets(filebuffer,1024,ifp);
  ior = strdup(filebuffer);

  fclose(ifp);
 
  akka_client = CORBA_ORB_string_to_object(orb, ior, &ev);
  if (!akka_client) {
    fprintf(stderr, "Cannot bind to %s\n", ior);
    return;
  }


  Akka_removeConsole(akka_client,cnum,&ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    fprintf(stderr, "Exception %d in removeconsole!\n", ev._major);
    return;
  }
  
  /* Clean up */
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);
}


void add_console(int cnum) {
  int argc = 1;
  char *argv[] = {""};
  
  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE * ifp;
  char * ior;
  char filebuffer[1024];
  
  CORBA_exception_init(&ev);
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  ifp = fopen("/tmp/.akka.ior","r");
  if( ifp == NULL ) {
    perror("No .akka.ior file");
    exit(EXIT_FAILURE);
  }

  fgets(filebuffer,1024,ifp);
  ior = strdup(filebuffer);

  fclose(ifp);
 
  akka_client = CORBA_ORB_string_to_object(orb, ior, &ev);
  if (!akka_client) {
    fprintf(stderr, "Cannot bind to %s\n", ior);
    return;
  }


  Akka_addConsole(akka_client,cnum,&ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    fprintf(stderr, "Exception %d in addconsole!\n", ev._major);
    return;
  }
  
  /* Clean up */
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);
  
}


void terminate_server() {
  int argc = 1;
  char *argv[] = {""};

  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE * ifp;
  char * ior;
  char filebuffer[1024];
  
  CORBA_exception_init(&ev);
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  ifp = fopen("/tmp/.akka.ior","r");
  if( ifp == NULL ) {
    perror("No .akka.ior file");
    exit(EXIT_FAILURE);
  }

  fgets(filebuffer,1024,ifp);
  ior = strdup(filebuffer);

  fclose(ifp);
 
  akka_client = CORBA_ORB_string_to_object(orb, ior, &ev);
  if (!akka_client) {
    fprintf(stderr, "Cannot bind to %s\n", ior);
    return;
  }


  Akka_Terminate(akka_client, &ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    fprintf(stderr, "Exception %d from terminate_server!\n", ev._major);
    return;
  }
  
  /* Clean up */
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);
}


void process_glyph(int cnum, int doglyph) {
  int argc = 1;
  char *argv[] = {""};

  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE * ifp;
  char * ior;
  char filebuffer[1024];
  
  CORBA_exception_init(&ev);
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  ifp = fopen("/tmp/.akka.ior","r");
  if( ifp == NULL ) {
    perror("No .akka.ior file");
    exit(EXIT_FAILURE);
  }

  fgets(filebuffer,1024,ifp);
  ior = strdup(filebuffer);

  fclose(ifp);
 
  akka_client = CORBA_ORB_string_to_object(orb, ior, &ev);
  if (!akka_client) {
    fprintf(stderr, "Cannot bind to %s\n", ior);
    return;
  }


  Akka_processGlyph(akka_client, cnum, doglyph,&ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    fprintf(stderr, "Exception %d from processglyph!\n", ev._major);
    return;
  }
  
  /* Clean up */
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);
}

void load_glyph_map(int cnum,
		    int glyph,
		    int lone,
		    int first,
		    int mid,
		    int last) {
  int argc = 1;
  char *argv[] = {""};

  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE * ifp;
  char * ior;
  char filebuffer[1024];
  
  CORBA_exception_init(&ev);
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  ifp = fopen("/tmp/.akka.ior","r");
  if( ifp == NULL ) {
    perror("No .akka.ior file");
    exit(EXIT_FAILURE);
  }

  fgets(filebuffer,1024,ifp);
  ior = strdup(filebuffer);

  fclose(ifp);
 
  akka_client = CORBA_ORB_string_to_object(orb, ior, &ev);
  if (!akka_client) {
    fprintf(stderr, "Cannot bind to %s\n", ior);
    return;
  }


  Akka_loadGlyphMap(akka_client, cnum, glyph, lone, first, mid, last, &ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    fprintf(stderr, "Exception %d in loadglyphmap!\n", ev._major);
    return;
  }
  
  /* Clean up */
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);
}

void set_keymap(int cnum,
		char *kbpath1,
		char *kbpath2) {
  int argc = 1;
  char *argv[] = {""};

  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE * ifp;
  char * ior;
  char filebuffer[1024];
  
  CORBA_exception_init(&ev);
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  ifp = fopen("/tmp/.akka.ior","r");
  if( ifp == NULL ) {
    perror("No .akka.ior file");
    exit(EXIT_FAILURE);
  }

  fgets(filebuffer,1024,ifp);
  ior = strdup(filebuffer);

  fclose(ifp);
 
  akka_client = CORBA_ORB_string_to_object(orb, ior, &ev);
  if (!akka_client) {
    fprintf(stderr, "Cannot bind to %s\n", ior);
    return;
  }


  Akka_setKeyboards(akka_client, cnum, kbpath1, kbpath2, &ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    fprintf(stderr, "Exception %d in setKeyboards!\n", ev._major);
    return;
  }
  
  /* Clean up */
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);
}

void set_default_keymap(char *kbpath) {
  int argc = 1;
  char *argv[] = {""};

  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE * ifp;
  char * ior;
  char filebuffer[1024];
  
  CORBA_exception_init(&ev);
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  ifp = fopen("/tmp/.akka.ior","r");
  if( ifp == NULL ) {
    perror("No .akka.ior file");
    exit(EXIT_FAILURE);
  }

  fgets(filebuffer,1024,ifp);
  ior = strdup(filebuffer);

  fclose(ifp);
 
  akka_client = CORBA_ORB_string_to_object(orb, ior, &ev);
  if (!akka_client) {
    fprintf(stderr, "Cannot bind to %s\n", ior);
    return;
  }

  Akka_setDefaultKB(akka_client, kbpath, &ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    fprintf(stderr, "Exception %d in setDefaultKB!\n", ev._major);
    return;
  }
  
  /* Clean up */
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);
}
