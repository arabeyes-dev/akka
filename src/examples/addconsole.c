/*****************************************************************************************/
/* addconsole for sh                                                                  */
/*****************************************************************************************/


#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "orb/orbit.h"
#include "akka.h"

Akka akka_client;


int main(int argc, char *argv[]) {

  CORBA_Environment ev;
  CORBA_ORB orb;

  FILE * ifp;
  char * ior;
  char filebuffer[1024];

  if (argc!=2) {
    fprintf(stderr, "Usage: addconsole n\nWhere n is the console number.\n");
    return -1;
  }

  CORBA_exception_init(&ev);
  orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    printf("We got exception %d in addConsole!\n", ev._major);
    return 1;
  }

  ifp = fopen("/tmp/.akka.ior","r");
  if( ifp == NULL ) {
    perror("No .akka.ior file!");
    exit(EXIT_FAILURE);
  }

  fgets(filebuffer,1024,ifp);
  ior = g_strdup(filebuffer);

  fclose(ifp);
 
  akka_client = CORBA_ORB_string_to_object(orb, ior, &ev);

  if (!akka_client) {
    fprintf(stderr, "Cannot bind to %s\n", ior);
    if(ev._major != CORBA_NO_EXCEPTION)
      fprintf(stderr, "Exception %d in addConsole!\n", ev._major);
    return 1;
  }
  
 
  Akka_addConsole(akka_client, atoi(argv[1]), &ev);

  if(ev._major != CORBA_NO_EXCEPTION) {
    fprintf(stderr, "Exception %d in addConsole after call!\n", ev._major);
    return 1;
  }
  
  /* Clean up */
  CORBA_Object_release(akka_client, &ev);
  CORBA_Object_release((CORBA_Object)orb, &ev);
  
  return 0;
}
