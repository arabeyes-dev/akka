/*
 * acon a utility to facilitate the right to left writing
 * Copyright 1999 Ahmed Abdel-Hamid Mohamed <ahmedam@mail.usa.com>
 *
 * Akka is more general purpose console server originally based on acon
 * Chahine Hamila (hchahine@magic.fr) 2000, 2001.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 *
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#ifdef __linux__
  #include <linux/kd.h>
  #include <linux/vt.h>
#endif

#ifdef __FreeBSD__
  #include <sys/consio.h>
#endif

#include "akkaperms.h"
#include "render.h"
#include "font.h"
#include "child.h"
#include "config.h"


//@{
/** @memo child.c variables.
 */
/// terminate the server
int terminate=0;
/// list of monitored consoles
int consoles[63];
/// number of consoles
int consolesn=0;
/// activate unicode use on current console
int useunicode=0;
/// akka output tty file descriptor
int akkatty;
/// monitored input tty file decriptor
int childtty;
/// akka output vcsa device file descriptor
int akkavcsa;
/// monitored input vcsa device file descriptor
int childvcsa;
/// current active monitored tty file descriptor
int akkattyn=-1;
/// current active monitored console number
int consolen;
/// refresh counter
int z;
/// VT switch occured -- key press
int changed=1; int keystroke=0;
/// loadkeys program
const char * loadkeys = LOADKEYS;
/// list of keymaps used by acon
char default_keyboard[500];
/// block cursor mode activated
int block_cursor=0;
/// inside escape sequence loop
int in_esc_seq=0;
//@}

//@{
/** Carry data about each monitored console.
 */
struct {
  /// approximate bidi in screen
  int bidiapprox;
  /// character encoding for bidi approximation
  int charencoding;
  /// general screen direction
  int direction;
  /// 1=Enable UTF-8
  int unicode;
  /// glyph filters for console
  char (*glyph_table)[256][4];
  /// keyboards
  char keyboard[2][500];
  /// index of currently loaded keymap
  int kmap_pointer;
}condata[63];
//@}

void loadkmap(char *kmap) {
  char buffer[500];
  if ((kmap) && (strlen(kmap)+strlen(loadkeys)+12)<(sizeof(buffer))) {
    sprintf(buffer, "%s %s &>/dev/null", loadkeys, kmap);
    if (system(buffer))
      fprintf(stderr, "Could not execute %s\n", loadkeys);
  }
  return;
}

void setconsolekeymap(int cnum, char *keymap1, char *keymap2) {
  if ((keymap1) && (keymap2) &&
      (strlen(keymap1)<500) && (strlen(keymap2)<500)) { 
    if (cnum==-1) 
      cnum = getstructnum(getconsolenum());
    else
      cnum = getstructnum(cnum);
    strcpy(condata[cnum].keyboard[0], keymap1);
    strcpy(condata[cnum].keyboard[1], keymap2);
    if (cnum==getstructnum(getconsolenum()))
      loadkmap(condata[cnum].keyboard[condata[cnum].kmap_pointer]);
  }
  return;
}

inline void terminate_server(void) {
  terminate = 1;
}

inline void setdefaultkeymap(char *defkeymap) {
  if ((defkeymap) && (strlen(defkeymap)<sizeof(default_keyboard)))
    strcpy(default_keyboard, defkeymap);
}


/** Check if the console is in the list.
    @param cnum VT number
    @return entry number+1 (true) if console is in list, else 0 (false).
*/
int isinlist(int cnum)
{
  int i;
  for(i=0;i<consolesn;i++)
    if(consoles[i]==cnum)return i+1;
  return 0;
}

/** Add new console to the list of the consoles to be monitored.
    @param cnum VT to be added
*/
int addconsole(int cnum)
{
  if(isinlist(cnum))return -1;
  if(consolesn==63)return -2;
  memset(&condata[consolesn],0,sizeof(*condata));
  condata[consolesn].glyph_table=NULL;
  condata[consolesn].kmap_pointer = 0;
  consoles[consolesn++]=cnum;
  
  return consolesn-1;
}


/* Remove a console from the list of the consoles to be monitored.
   @param cnum VT to be removed
*/
void removeconsole(int cnum)
{
  int i;
  int cons = cnum;
  cnum=getstructnum(cnum);
  if (cnum>=0) {
    setactive(cons);
    for(i=cnum;i<consolesn-1;i++)
      {
	memcpy(&condata[i],&condata[i+1],sizeof(condata[0]));
	consoles[i]=consoles[i+1];
      }
    consolesn--;
  }
  return;
}

/** Turn off echo and send each character
    @param fd open terminal file descriptor
 */
int setfd(int fd)
{
  struct termios newt;
  tcgetattr(fd, &newt);
  
  newt.c_lflag &= ~ (ICANON | ECHO | ISIG); /* Disable canonical mode, disallow echoing 
					       of input characters and ignore input signals (^C, ^D, etc...) */
  newt.c_iflag = 0;
  
  tcsetattr(fd, TCSAFLUSH, &newt);
  
  /*Set meta bit*/
  seteuid(akka_id);
  /* Tells the console to generate escape sequences with the
     meta key */
  
  /* I can't figure out if there is at all, an equiv of this under
     FreeBSD. So, temporarily we will just ignore this under FreeBSD
     since, according to my understanding, this isn't really important
  */
#ifdef __linux__
  if(ioctl(fd,KDSKBMETA,(long)K_METABIT))       /* Linux: set meta key handling mode */
    perror("Error setfd::KDSKBMETA");
#endif
  seteuid(user_id);
  return 0;
}

/** Get pressed key
    @param fd open terminal file descriptor
    @param time time to wait in �s
    @return key code if any is pressed, else 256
*/
unsigned int getkey(int fd,int time)
{
  fd_set fdset;
  unsigned char chr;
  struct timeval tv;
  
  FD_ZERO(&fdset);
  FD_SET(fd,&fdset);
  
  tv.tv_sec=0;
  tv.tv_usec=time;
  
  if(!select(fd+1,&fdset,NULL,NULL,&tv))
    return 256;
  read(fd,&chr,1);
  return chr;
}

/** Get current active console.
    @return current active console, -1 if in graphic mode
*/
int getactive(void)
{
  static int consolefd=0;
  long ingraphics;
  struct {ushort active;ushort signal;ushort state;}argp;

  seteuid(akka_id);
  if((consolefd=open("/dev/console",O_RDONLY))==-1){
    perror("Cannot open /dev/console");
    seteuid(user_id);
    return -1;
  }
  
#ifdef __linux__
  if(ioctl(consolefd,VT_GETSTATE,&argp))          /* Linux: get state of current tty */
    {
      perror("Error in child.c::getactive:VT_GETSTATE");
      seteuid(user_id);
      return -1;
    }
#endif
  
#ifdef __FreeBSD__
  if (ioctl(consolefd, TIOCMGET, &argp))          /* FreeBSD: get state of current tty */
  {
    perror("Error in child.c::getactive:TIOCMGET");
    seteuid(user_id);
    return -1;
  }
#endif
  
#if defined(__FreeBSD__) || defined(__linux__)
  if(ioctl(consolefd,KDGETMODE,&ingraphics))      /* Linux/FreeBSD: get current mode of terminal */
    {
      perror("Error in child.c::getactive:KDGETMODE");
      seteuid(user_id);
      return -1;
    }
#endif
  
  close(consolefd); /* To stop 'Too many open files' error. Needed elsewhere?*/
  seteuid(user_id);
  if(ingraphics) return -1;	/*in graphics mode*/
  return argp.active;
}

/** Simulate a keypress.
    @param chr keypress to simulate on the term
*/
int sendkey(unsigned char chr)
{
  char buf[200];
  
  seteuid(akka_id);
  if (ioctl(childtty,TIOCSTI,&chr)) /*Send the key*/
    {
      /*Try to reopen akkatty*/
      close(childvcsa);
      close(childtty);
      
      sprintf(buf,"/dev/tty%d",consolen);
      if((childtty=open(buf,O_RDWR))==-1)
	{
	  fprintf(stderr, "Error opening %s: %s\n",buf, strerror(errno));
	  seteuid(user_id);
	  return -1;
	}
      sprintf(buf,"/dev/vcsa%d",consolen);
      if((childvcsa=open(buf,O_RDWR))==-1)
	{
	  fprintf(stderr, "Error opening %s: %s\n",buf, strerror(errno));
	  seteuid(user_id);
	  return -1;
	}
      
      if(ioctl(childtty,TIOCSTI,&chr))
	{
	  perror("Error in child.c:TIOCSTI");
	  seteuid(user_id);
	  return -1;
	}
      seteuid(user_id);
    }
  return 0;
}

/** Triggered by keycode 88
 */
void switch_keymap(void) {
  int cnum = getstructnum(consolen);
  condata[cnum].kmap_pointer++;
  condata[cnum].kmap_pointer%=2;
  loadkmap(condata[cnum].keyboard[condata[cnum].kmap_pointer]);
  return;
}

/** Get input and send it to the active console.
    Some codes are reserved.
    86 changes direction (on the PC samples it's F12)
    87 blocks the cursor (on the PC samples it's F10)
    88 switches the keyboard (on the PC samples it's F11)
    @param time time to wait for the key press in �s
*/
int processkey(int time)
{
  unsigned int chr;
  if((chr=getkey(akkatty,time))!=256)
    {
      changed=1; keystroke=1;
      if(chr==0x86) setdirection(-1, (direction?0:1));
      else if(chr==0x88) switch_keymap();
      else if(chr==0x87) block_cursor=block_cursor?0:1;
      
      /* unicode management should be tested */
      else if(useunicode && chr>0xa0)
	{
	  int c;
	  c=chr-0xa0+0x600;
	  
	  sendkey(0xc0 | (c>>6) );
	  sendkey(0x80 | (c & 0x3f));
	}
      else {
	sendkey(chr);
	
	/* Here's what we do now to detect the addition of
	   a letter.
	   
	   Note that this is a heuristic, that it can be flawed
	   in some circumstances. It is possible to have a fully 
	   functioning algorithm but this would mean redirecting 
	   the whole keyboard management from the kernel.
	   
	   It would be much easier to simply integrate akka into the
	   kernel.This is just about exploring concepts.
	   
	   *********************************************************
	   The heuristic now:
	   we are going to save the last cursor position everytime we go through
	   this loop.
	   The next loop, we will try to detect a change there.
	   If there's a change, it is likely that a key made it change, so 
	   we will consider it to be due to a keystroke.
	   In that case, we'll use the charcode to detect the kind of character
	   entered and decide if we are in a opposing direction or not.
	   If we are in an opposing direction, we block the cursor.
	   A counter will have to remember how many characters were entered 
	   in reverse direction to move the cursor in that direction as many 
	   time as needed. There is no other way to do it because the end character
	   or control e has too different meanings from an app to another.
	   *********************************************************
	   
	   I'm dropping the bidi input management altogether for the
	   following reasons:
	   - the heuristic used is not always accurate. Therefore the complexity 
	   added for its implementation is not worth it,
	   - there are many orders for entering numbers according to the dialect
	   used (classical, standard, regional...). That leaves us with the only
	   problem of entering one latin word at a time. At worst, the block cursor
	   mode should be enough
	*/
	
	if (block_cursor) {
	  if ((!in_esc_seq) && (((chr>31) && (chr<128)) || (chr>160))){
	    sendkey('\033');
	    sendkey('[');
	    sendkey('D');
	  } else if (chr==27) {
	    in_esc_seq=3;
	  } else if (in_esc_seq) 
	    in_esc_seq--;
	}
      }
    }
  return 0;
}

void setdirection(int cnum, int dir)
{
  if (cnum==-1) 
    cnum = getstructnum(getconsolenum());
  else
    cnum = getstructnum(cnum);
  if (cnum>=0) {
    if (cnum==getstructnum(getconsolenum()))
      direction = dir;
    condata[cnum].direction=dir;
  }
  return;
}

void setbidiapprox(int cnum, int bda, int charset)
{
  if (cnum==-1) 
    cnum = getstructnum(getconsolenum());
  else
    cnum = getstructnum(cnum);
  if 
    (cnum>=0) {
    if (cnum==getstructnum(getconsolenum())) {
      bidiapprox = bda;
      charencoding = charset;
    }
    condata[cnum].bidiapprox=bda;
    condata[cnum].charencoding=charset;
  }
  return;
}

void setglyphrender(int cnum, int glyphrender)
{
  int counter;
  if (cnum==-1) 
    cnum = getstructnum(getconsolenum());
  else
    cnum = getstructnum(cnum);
  if 
    (cnum>=0) {
    if (glyphrender) {
      if (!(condata[cnum].glyph_table))
	condata[cnum].glyph_table = malloc(1024);
      for (counter=0; counter<256; counter++) {
	(*(condata[cnum].glyph_table))[counter][0]=0;
	(*(condata[cnum].glyph_table))[counter][1]=0;
	(*(condata[cnum].glyph_table))[counter][2]=0;
	(*(condata[cnum].glyph_table))[counter][3]=0;
      }
    } else {
      if (condata[cnum].glyph_table) {
	free(condata[cnum].glyph_table);
	condata[cnum].glyph_table = NULL;
      }
    }
    if (cnum==getstructnum(getconsolenum()))
      glyph_table = condata[cnum].glyph_table;
  }
  return;
}

void setglyphshapes(int cnum, int glyph, int lone, int first, int mid, int last)
{
  if (cnum==-1) 
    cnum = getstructnum(getconsolenum());
  else
    cnum = getstructnum(cnum);
  if 
    (cnum>=0) {
    /* There's something I don't understand in here, but it seems the positions
       of col 1 and 3 are swapped... */
    if (condata[cnum].glyph_table) {
      (*(condata[cnum].glyph_table))[glyph][0]=lone;
      (*(condata[cnum].glyph_table))[glyph][1]=last;
      (*(condata[cnum].glyph_table))[glyph][2]=mid;
      (*(condata[cnum].glyph_table))[glyph][3]=first;
    }
  }
  return;
}

int setactive(int console)
{
  seteuid(akka_id);

#if defined(__FreeBSD__) || defined(__linux__)
  if(ioctl(akkatty,VT_ACTIVATE,console))      /* Linux/FreeBSD: switch to the "console" vt */
    {
      perror("Error VT_ACTIVATE");
      seteuid(user_id);
      return -1;
    }
#endif
  
  seteuid(user_id);
  return 0;
}

/*Switch to ttyn, set direction*/
int initconsole(int ttyn,int cons)
{
  char code[3];  
  cons=getstructnum(cons);
  
  if (cons!=-1) {
    direction=condata[cons].direction;
    bidiapprox=condata[cons].bidiapprox;
    charencoding=condata[cons].charencoding;
    useunicode=condata[cons].unicode;
    glyph_table=condata[cons].glyph_table;
    loadkmap(condata[cons].keyboard[condata[cons].kmap_pointer]);
  }
  
  /* I have no idea what these terminal control 
     sequences do and why they're here. I tried 
     to remove them and it didn't change anything
     on my computers. I will leave them though as I
     don't know if they might be necessary on some
     machines. */

  code[0]=27;
  code[1]='%';
  code[2]=(condata[cons].unicode)?'G':'@';
  write(childtty,code,3);
  
  /* switch consoles */
  /* done in font.c: setactive(ttyn); */
  
  return 0;    
}


/*The main loop*/
int runchild(int ttyn)
{
  char buf[100];
  char monitored = 0; // last loop was done on a monitored console
  
  signal(SIGTERM,terminate_sig);
  signal(SIGHUP,terminate_sig);
  signal(SIGINT,terminate_sig);
  signal(SIGQUIT,terminate_sig);
  signal(SIGILL,terminate_sig);
  signal(SIGSEGV,terminate_sig);
  signal(SIGBUS,terminate_sig);

  sprintf(buf,"/dev/tty%d",ttyn);
  
  seteuid(akka_id);
  if((akkatty=open(buf,O_RDWR | O_NONBLOCK ))==-1)
    {
      fprintf(stderr, "Error opening %s: %s\n",buf,strerror(errno));
      seteuid(user_id);
      return -1;
    }
  sprintf(buf,"/dev/vcsa%d",ttyn);
  if((akkavcsa=open(buf,O_RDWR))==-1)
    {
      fprintf(stderr, "Error opening %s: %s\n",buf,strerror(errno));
      seteuid(user_id);
      return -1;
    }
  seteuid(user_id);
  
  setfd(akkatty);
  tcsetpgrp(akkatty,getpid());
  
  z=0;
  akkattyn=ttyn;
  
  while(!terminate)
    {
      /* We must detect changes here in case we switch to 
	 a console that's not monitored */
      while ((!isinlist(getactive())) && (!terminate))
	{
	  if (monitored) {
	    monitored = 0;
	    loadkmap(default_keyboard);
	  }
	  sleep(1);
	}
      
      consolen=getactive();
      seteuid(akka_id);
      
      sprintf(buf,"/dev/tty%d",consolen);
      if((childtty=open(buf,O_RDWR))==-1)
	{
	  fprintf(stderr, "Error opening %s: %s\n",buf,strerror(errno));
	  seteuid(user_id);
	  return -1;
	}
      sprintf(buf,"/dev/vcsa%d",consolen);
      if((childvcsa=open(buf,O_RDWR))==-1)
	{
	  fprintf(stderr, "Error opening %s: %s\n",buf,strerror(errno));
	  seteuid(user_id);
	  return -1;
	}
      seteuid(user_id);
      
      initconsole(ttyn,consolen);	/*Switch console*/
      copy_console_settings(childtty, akkatty, consolen, ttyn);
      monitored = 1;
      
      while(consolesn && getactive()==ttyn && !terminate)
	{
	  if((z++)%6==0 || changed)
	    {
	      changed=0;
	      drawscrn(akkavcsa,childvcsa);
	    }
	  processkey(20000);
	  seteuid(user_id);
	}
      
      /*close devices*/
      close(childtty);
      close(childvcsa);
    }
  
  cleanup();
  
  close(akkavcsa);
  close(akkatty);
  return 0;
}

void cleanup(void)
{
  /* switch consoles */
  if(getactive()==akkattyn)
    setactive(consolen);
  printf("Cleaning...\n");
  loadkmap(default_keyboard);
  printf("Akka is exited\n");
  return;
}

void terminate_sig(int sig)
{
  signal(SIGTERM,SIG_IGN);
  signal(SIGHUP,SIG_IGN);
  signal(SIGINT,SIG_IGN);
  signal(SIGQUIT,SIG_IGN);
  signal(SIGILL,SIG_IGN);
  signal(SIGSEGV,SIG_IGN);
  signal(SIGBUS,SIG_IGN);

  cleanup();
  exit(1);
}

