/*
 * acon a utility to facilitate the right to left writing
 * Copyright 1999 Ahmed Abdel-Hamid Mohamed <ahmedam@mail.usa.com>
 *
 * Akka is more general purpose console server originally based on acon
 * Chahine Hamila (hchahine@magic.fr) 2000, 2001.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 *
 */
 
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <endian.h>

#define FRIBIDI_INTERFACE_1  
#include <fribidi/fribidi.h>

#include "akkaperms.h"
#include "child.h"
#include "render.h"


int bidiapprox=0;
int charencoding=FRIBIDI_CHARSET_ISO8859_6; /* default is ISO-8859-6 */
int direction=1;	     /* 0=left to right, 1=right to left */
int curpos;                 /* Cursor position in bidi approximation mode */
int savecurpos;              /* We are in the y where the cursor is located */
char (*glyph_table)[256][4]; /* Filter for the current screen. Allows encoding conversions
				and glyph rendering */

struct {
  unsigned char lines, cols, x, y;
} scrn; /*carries console size and current cursor position*/

void mirror(char *line, int len) {
  int i,j;
  char ctmp, atmp;
  for (i=0, j=len-2; i<j; i+=2, j-=2) {
    ctmp = line[i];
    atmp = line[i+1];
    line[i]=line[j];
    line[i+1]=line[j+1];
    line[j]=ctmp;
    line[j+1]=atmp;
  }
}

/* WARNING: I THINK THERE IS AN ENDIAN ISSUE HERE */

/*
  The glyph rendering process use a table which has the following format:
  Entries that do not exist will be marked by a null (0) position.
  index : position in character set
  col 0 : font position for lone character
  col 1 : font position for first char
  col 2 : font position for middle char
  col 3 : font position for last char
  For example, a character that doesn't attach to the next one will 
  have 0 in its entries 1 and 2, since it can't have a "first" or
  "middle" position glyph.
  Characters like lone hamza or numbers for example, 
  existing only in a lone form don't have to be specified.
  The default will indeed be a lone character that does attach to
  neither previous or next one.
*/
void glyph_render(unsigned char *line2render, int len) {
  int last_shape;
  int torender;
  int current;

#if __BYTE_ORDER == __LITTLE_ENDIAN

  if ((*glyph_table)[line2render[1]][0])
    last_shape=1;
  else
    last_shape=0;
  for (current=2; current<len; current+=2) {
    /* The glyph rendering algorithm is a one pass algorithm.
       The rendered character will always be the previous one,
       for which we're supposed to have enough information in 
       the loop's body. */
    if (!((*glyph_table)[line2render[current]][2]
	  || (*glyph_table)[line2render[current]][3]))
      {
	/* The current character has no middle or last form so
	   it doesn't attach to both previous and next.
	   If the previous is supposed to attach we'll correct
	   it and set it to the "end" or "lone" shape. */ 
	if ((last_shape==2) && ((*glyph_table)[line2render[current-2]][3]))
	  torender=3;
	else
	  torender=0;
	if ((*glyph_table)[line2render[current]][1])
	  last_shape=1;
	else
	  last_shape=0;
      } else {
	torender=last_shape;
	/* the current character has a middle or last form
	   so the last one will attach if it can.
	   If it can't, lone or first form will be chosen.
	*/
	if ((torender==0) || (torender==3)) { /* can't attach to previous */
	  if ((*glyph_table)[line2render[current]][1])
	    last_shape=1;
	  else
	    last_shape=0;
	} else {
	  if ((*glyph_table)[line2render[current]][2])
	    last_shape=2;
	  else
	    last_shape=3;
	};
      }
    if ((*glyph_table)[line2render[current-2]][torender]) 
      line2render[current-2]=((*glyph_table)[line2render[current-2]][torender]);
  }
  
#else
  if ((*glyph_table)[line2render[1]][1])
    last_shape=1;
  else
    last_shape=0;
  for (current=3; current<len; current+=2) {
    /* The glyph rendering algorithm is a one pass algorithm.
       The rendered character will always be the previous one,
       for which we're supposed to have enough information in 
       the loop's body. */
    if (!((*glyph_table)[line2render[current]][2]
	  || (*glyph_table)[line2render[current]][3]))
      {
	/* The current character has no middle or last form so
	   it doesn't attach to both previous and next.
	   If the previous is supposed to attach we'll correct
	   it and set it to the "end" or "lone" shape. */ 
	if ((last_shape==2) && ((*glyph_table)[line2render[current-2]][3]))
	  torender=3;
	else
	  torender=0;
	if ((*glyph_table)[line2render[current]][1])
	  last_shape=1;
	else
	  last_shape=0;
      } else {
	torender=last_shape;
	/* the current character has a middle or last form
	   so the last one will attach if it can.
	   If it can't, lone or first form will be chosen.
	*/
	if ((torender==0) || (torender==3)) { /* can't attach to previous */
	  if ((*glyph_table)[line2render[current]][1])
	    last_shape=1;
	  else
	    last_shape=0;
	} else {
	  if ((*glyph_table)[line2render[current]][2])
	    last_shape=2;
	  else
	    last_shape=3;
	};
      }
    if ((*glyph_table)[line2render[current-2]][torender]) 
      line2render[current-2]=((*glyph_table)[line2render[current-2]][torender]);
  }
  
  if (current==len) { /* this one is Endian dependent I think */
    if (!(*glyph_table)[line2render[current]][3])
      {
	  /* The current character has no middle or last form so
	     it doesn't attach to both previous and next.
	     If the previous is supposed to attach we'll correct
	     it and set it to the "end" or "lone" shape. */ 
	  if (last_shape==2) {
	    if ((*glyph_table)[line2render[current-2]][3])
	      torender=3;
	    else
	      torender=0;
	  } else
	    torender=0;
	  if ((*glyph_table)[line2render[current]][1])
	    last_shape=1;
	  else
	    last_shape=0;
      } else 
	torender=last_shape;
    if ((*glyph_table)[line2render[current-2]][torender])
      line2render[current-2]=(*glyph_table)[line2render[current-2]][torender];
  }
#endif
}


/* dirty but easy, global vars added for cursor position computation */
int l, r; /* left and right margin */
void swapmargins(char *line, int len) {
  int i, c;
  char line2render[1024]; /* anyone has lines bigger than 512 columns? ;) */
  if (len>=1024) {
    printf("screen too wide, please report this to mch@chaham.com\n");
    return;
  }
#if __BYTE_ORDER == __LITTLE_ENDIAN
  for (i=0; i<len; i+=2)
    if (!line[i])
      line[i]=' ';
  i=0;
  while ((isspace(line[i])) && (i<len)) {
    i+=2;
  };
  if (i>=len)
    return;
  l=i;
  i=len-2;
  while ((isspace(line[i])) && (i>0)) {
    i-=2;
  };
  r=len-i;
  for (i=0, c=0; c<r-2; i+=2, c+=2) {
    line2render[c]=32;
    line2render[c+1]=line[len-r+i+1];
  }
  for (i=l; c<len-l; i+=2, c+=2) {
    line2render[c]=line[i];
    line2render[c+1]=line[i+1];
  }
  for (i=0; i<l; c+=2, i+=2) {
    line2render[c]=32;
    line2render[c+1]=line[i+1];
  }
#else
  for (i=1; i<len; i+=2) /* this is probably endian dependent as well */
    if (!line[i])
      line[i]=' '; /* convert null character to space for later treatments like bidi */
  i=1;
  while ((isspace(line[i])) && (i<len)) {
    i+=2;
  };
  if (i>=len)
    return;
  l=i;
  i=len-1;
  while ((isspace(line[i])) && (i>0)) {
    i-=2;
  };
  r=len-i;
  for (i=0, c=1; c<r-2; i+=2, c+=2) {
    line2render[c]=32;
    line2render[c-1]=line[len-r+i-1];
  }
  for (i=l; c<len-l; i+=2, c+=2) {
    line2render[c]=line[i];
    line2render[c-1]=line[i-1];
  }
  for (i=0; i<l; c+=2, i+=2) {
    line2render[c]=32;
    line2render[c-1]=line[i];
  }
#endif
  for (c=0; c<len; c++)
    line[c]=line2render[c];
}

/* dirty, but easy, globalvar for current cursor's position */
int current_cur_pos;
void approximatebidi(char *line, int len) {
  char line2render[1024]; /* anyone has lines bigger than 512 columns? ;) */
  int i;
  FriBidiChar inputline[1024];
  FriBidiChar outputline[1024];
  FriBidiCharType basedir;
  FriBidiStrIndex positionVtoL[1024];
  FriBidiStrIndex positionLtoV[1024];
  FriBidiLevel embedding_list[1024];
  if (len>=1024) {
    printf("screen too wide, please report this to mch@chaham.com\n");
    return;
  }
#if __BYTE_ORDER == __LITTLE_ENDIAN
  for (i=0; i<len; i+=2)
    line2render[i>>1]=line[i];
  line2render[i>>1]=0;
#else
  for (i=1; i<len; i+=2)
    line2render[(i-1)>>1]=line[i];
  line2render[(i-1)>>1]=0;
#endif
  switch ((FriBidiCharSet) charencoding) {
  case FRIBIDI_CHARSET_ISO8859_6: /* default, but added for speed */
    fribidi_iso8859_6_to_unicode(line2render, inputline);
    break;
  case FRIBIDI_CHARSET_CP1256:
    fribidi_cp1256_to_unicode(line2render, inputline);
    break;
  case FRIBIDI_CHARSET_ISIRI_3342:
    fribidi_isiri_3342_to_unicode(line2render, inputline);
    break;
  default:
    fribidi_iso8859_6_to_unicode(line2render, inputline);
  }
  basedir = FRIBIDI_TYPE_ON;
  fribidi_log2vis (inputline, len>>1, &basedir,
		   outputline, positionVtoL, positionLtoV, embedding_list);
 switch ((FriBidiCharSet) charencoding) {
  case FRIBIDI_CHARSET_ISO8859_6: /* default, but added for speed */
    fribidi_unicode_to_iso8859_6(outputline, len>>1, line2render);
    break;
  case FRIBIDI_CHARSET_CP1256:
    fribidi_unicode_to_cp1256(outputline, len>>1, line2render);
    break;
  case FRIBIDI_CHARSET_ISIRI_3342:
    fribidi_unicode_to_isiri_3342(outputline, len>>1, line2render);
    break;
  default:
    fribidi_unicode_to_iso8859_6(outputline, len>>1, line2render);
  }
 if (savecurpos) {
   curpos=positionLtoV[current_cur_pos-1];
   if (embedding_list[current_cur_pos-1] & 1) /* RTL char */
     curpos -=1;
   else
     curpos +=1;
 }
#if __BYTE_ORDER == __LITTLE_ENDIAN
 for (i=0; i<len; i+=2)
   line[i]=line2render[i>>1];
#else
 for (i=1; i<len; i+=2)
   line[i]=line2render[(i-1)>>1];
#endif
};

void processline(unsigned char *line,int len)
{
  if (!bidiapprox) {
    if (direction) 
      mirror(line, len);
  } else {
    approximatebidi(line, len);
    if (direction) {
      swapmargins(line, len);
      if (savecurpos) {
	curpos = curpos +(r>>1) -1 -(l>>1);
      }
    }
  }
  if (glyph_table) 
    glyph_render(line, len);
}

int drawscrn(int ttyvc,int consolevc)
{
	int i;
	char line[400];
   
	lseek(consolevc,0,SEEK_SET);
	lseek(ttyvc,4,SEEK_SET);

	read(consolevc,&scrn,4);
           
	for(i=0;i<scrn.lines;i++)
	{
	  /* dirty, but easy: global vars bidi */
	  if ((i==scrn.y) && keystroke) {
	    savecurpos = 1;
	    current_cur_pos=scrn.x;
	    keystroke=0;
	  }
	  else
	    savecurpos = 0;
	  read (consolevc,line,scrn.cols*2);
	  processline (line,scrn.cols*2);
	  write(ttyvc,line,scrn.cols*2);
	  processkey(0);
	}
	lseek(ttyvc,0,SEEK_SET);
	
	/* FIXME */
       	if (bidiapprox)
	  scrn.x=curpos;
	else if (direction)
	  scrn.x=scrn.cols-scrn.x-1;
	write(ttyvc,&scrn,4); 
	return 0;
}
