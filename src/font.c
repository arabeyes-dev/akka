/*
 * acon a utility to facilitate the right to left writing
 * Copyright 1999 Ahmed Abdel-Hamid Mohamed <ahmedam@mail.usa.com>
 *
 * Akka is more general purpose console server originally based on acon
 * Chahine Hamila (hchahine@magic.fr) 2000, 2001.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 *
 */

#include <errno.h>
#include <stdio.h>
#include <sys/ioctl.h>

#ifdef __linux__
  #include <linux/kd.h>
#endif

#include "akkaperms.h"
#include "child.h"


int result;

struct consolefontdesc fontdesc;
char fontdata[32*512];
void getfont(int tty)
{
  fontdesc.charcount=512;
  fontdesc.chardata=fontdata;
  
  seteuid(akka_id);
  result=ioctl(tty,GIO_FONTX,&fontdesc);    /* Linux: get font */
  seteuid(user_id);
  if(result)
    perror("Akka: can't get font");
  return;
}

void setfont(int tty)
{
  seteuid(akka_id);
  result = ioctl(tty,PIO_FONTX,&fontdesc);   /* Linux: set font */
  seteuid(user_id);
  if(result)
    perror("Akka: can't set font");
  return;
}

char cmap[48];
void getcmap(int tty) {
  seteuid(akka_id);
  result = ioctl(tty,GIO_CMAP,cmap);   /* Linux: get color palette on VGA+ */
  seteuid(user_id);
  if(result)
    perror("Akka: can't get color map");
  return;
}

void setcmap(int tty) {
  seteuid(akka_id);
  result = ioctl(tty,PIO_CMAP,cmap);   /* Linux: set color palette on VGA+ */
  seteuid(user_id);
  if(result)
    perror("Akka: can't set color map");
  return;
}

char acm[E_TABSZ*sizeof(unsigned short)];
void getacm(int tty) {
  seteuid(akka_id);
  result = ioctl(tty,GIO_UNISCRNMAP,acm);   /* Linux: get full Unicode screen mapping */
  seteuid(user_id);
  if(result)
    perror("Akka: can't get acm");
  return;
}

void setacm(int tty) {
  seteuid(akka_id);
  result = ioctl(tty,PIO_UNISCRNMAP,acm);   /* Linux: set full Unicode screen mapping */
  seteuid(user_id);
  if(result)
    perror("Akka: can't set acm");
  return;
}

struct unipair ufm[1024];
struct unimapdesc umapdesc;
void getsfm(int tty)
{
  umapdesc.entry_ct = 1024;
  umapdesc.entries = ufm;
  
  seteuid(akka_id);
  result=ioctl(tty,GIO_UNIMAP,&umapdesc);   /* Linux: get unicode-to-font mapping from kernel */
  seteuid(user_id);
  if(result)
    perror("Akka: can't get sfm");
  return;
}

void setsfm(int tty)
{
  seteuid(akka_id);
  result = ioctl(tty,PIO_UNIMAP,&umapdesc);   /* Linux: set unicode-to-font mapping from kernel */
  seteuid(user_id);
  if(result)
    perror("Akka: can't set sfm");
  return;
}

void copy_console_settings(int from_tty, int to_tty, int from_cons, int to_cons) {
  /* copy font, acm, sfm */
  //  unnecessary?
  /* setactive(from_cons); though this is not specified AFAIK, it looks like 
			   the following functions would not work if not applied
			   to the active console
			*/
  getfont(from_tty);
  getacm(from_tty);
  getsfm(from_tty);
  setactive(to_cons);
  setfont(to_tty);
  setacm(to_tty);
  setsfm(to_tty);
  /* Tell me if I forgot anything (mch@chaham.com) */
  return;
}
