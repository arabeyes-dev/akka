/*
 * This file is the header for CORBA support in the Akka server
 * Chahine Hamila & Chokri Mraidha - 2000,2001
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 *
 */

/** Function starting the CORBA event loop thread.
    @param argc Must be the same as main's.
    @param argv Must be the same as main's.
*/
   
void start_corba_akka_server(int argc, char **argv);
