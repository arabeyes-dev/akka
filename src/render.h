/*
 * acon a utility to facilitate the right to left writing
 * Copyright 1999 Ahmed Abdel-Hamid Mohamed <ahmedam@mail.usa.com>
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 *
 */


extern int direction;
extern int dorender;
extern int bidiapprox;
extern int charencoding;
/*
  The glyph rendering process uses a table which has the following format:
  Lines starting by # are comments.
  index : position in character set
  col 0 : font position for lone character
  col 1 : font position for first char
  col 2 : font position for middle char
  col 3 : font position for last char
  The table contains 256x4 elements.
  Entries that do not exist will be marked by a null (0) position.
  For example, a character that doesn't attach to the next one will 
  have 0 in its entries 2 and 3, since it can't have a "first" or
  "middle" position glyph.
  Characters like lone hamza or numbers for example, 
  existing only in a lone form don't have to be specified.
  The default will indeed be a lone character that does attach to
  neither previous or next one.
  Since every monitored console have its own filter, the tables are dealt
  with dynamically. They are initialized on demand. A null pointer
  indicates a trivial mapping where glyph rendering is disabled.
*/
extern char (*glyph_table)[256][4];
int drawscrn(int ttyvc,int consolevc);
