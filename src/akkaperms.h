/*
 * acon a utility to facilitate the right to left writing
 * Copyright 1999 Ahmed Abdel-Hamid Mohamed <ahmedam@mail.usa.com>
 *
 * Akka is more general purpose console server originally based on acon
 * Chahine Hamila (hchahine@magic.fr) 2000, 2001.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 *
 */

/* Loading fonts, keymaps, and translation maps is not the responsibility
   of this program anymore.
   It should be done by scripts now.
*/

#define DATAPATH "/usr/lib/akka/"
#define get_ids() user_id=getuid(); akka_id=geteuid()

/// Console owner uid
int user_id;
/// Akka server uid (generally root)
int akka_id;

