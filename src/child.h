/*
 * acon is a utility to facilitate the right to left writing
 * Copyright 1999 Ahmed Abdel-Hamid Mohamed <ahmedam@mail.usa.com>
 * 
 * Akka is more general purpose console server originally based on acon
 * Chahine Hamila <mch@chaham.com> 2000, 2001.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 675 Mass
 * Ave, Cambridge, MA 02139, USA.
 *
 */

extern int consolesn;
extern int keystroke;

int addconsole(int cnum);
int getactive(void);
int isinlist(int cnum);
int processkey(int time);
int runchild(int vc);
int setactive(int console);

/*
inline int getconsolenum(void);
inline int getstructnum(int cons);
*/
#define getconsolenum() consolen
#define getstructnum(x) isinlist(x)-1
inline void terminate_server(void);

void removeconsole(int cnum);
void saveconfiguration(void);
void setbidiapprox(int cnum, int bda, int charset);
void setconsole(int cnum, char *keymap1, char *keymap2);
void setconsoledirection(int cnum,int direction);
void setconsolefont(int cnum,char *font);
void setconsolekeymap(int cnum, char *keymap1, char *keymap2);
void setconsoletranslation(int cnum,char *translation);
void setconsoleunicode(int cnum,int uni);
void setdefaultkeymap(char *defkeymap);
void setdirection(int cnum, int dir);
void setglyphrender(int cnum, int glyphrender);
void setglyphshapes(int cnum, int glyph, int lone, int first, int mid, int last);

void cleanup(void);
void terminate_sig(int) __attribute__((noreturn));

