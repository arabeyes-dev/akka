#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "orb/orbit.h"
#include "akka.h"
#include "child.h"


Akka akka_client = CORBA_OBJECT_NIL;

/* declaration of the meat of the process*/
/* Hindi numbers treatment removed. Context dependent treatment of numbers requires word processing abilities unsuited for character oriented terminals. So no context dependent treatment is done, rely on fonts for appearance. */


/************************************************
	     CORBA Server functions             
   Self explaining calls. For the corresponding
   functions refer to the wrapped equivalents
*************************************************/

void
do_setDirection(PortableServer_Servant servant,
		CORBA_short cnum,
		CORBA_short dir,
		CORBA_Environment *ev)
{
  setdirection(cnum,dir);
}

void
do_setBidiApproximation(PortableServer_Servant servant,
			CORBA_short cnum,
			CORBA_short bda,
			CORBA_short charset,
			CORBA_Environment *ev)
{
  setbidiapprox(cnum,bda,charset);
}

void
do_removeConsole(PortableServer_Servant servant,
		 CORBA_short cnum,
		 CORBA_Environment *ev)
{
  removeconsole(cnum);
}

void
do_addConsole(PortableServer_Servant servant,
	      CORBA_short cnum,
	      CORBA_Environment *ev) 
{
  addconsole(cnum);
}

void
do_processGlyph(PortableServer_Servant servant,
		CORBA_short cnum,
		CORBA_short doglyph,
		CORBA_Environment *ev) 
{
  setglyphrender(cnum, doglyph);
}

void
do_loadGlyphMap(PortableServer_Servant servant,
		CORBA_short cnum,
		CORBA_short glyph,
		CORBA_short lone,
		CORBA_short first,
		CORBA_short mid,
		CORBA_short last,
		CORBA_Environment *ev)
{
  /* This will check if we're not overwriting an existing
     glyph map table. If it's the case we'll just replace
     it. Otherwise, we create a new one. */
  setglyphshapes(cnum, glyph, lone, first, mid, last);
  
};

void
do_setKeyboards(PortableServer_Servant servant,
		CORBA_short cnum,
		const CORBA_char *kb_path1,
		const CORBA_char *kb_path2,
		CORBA_Environment *ev)
{
  setconsolekeymap((short int)cnum, (char *)kb_path1, (char *)kb_path2);
};

void
do_setDefaultKB(PortableServer_Servant servant,
		const CORBA_char *default_kb_path,
		CORBA_Environment *ev) {
  setdefaultkeymap(default_kb_path);
};

void
do_Terminate(PortableServer_Servant servant,
	     CORBA_Environment *ev) {
  terminate_server();
};



PortableServer_ServantBase__epv base_epv = {
  NULL,
  NULL,
  NULL
};

POA_Akka__epv akka_epv = { NULL,
			   do_setDirection,
			   do_setBidiApproximation,
			   do_removeConsole,
			   do_addConsole,
			   do_processGlyph,
			   do_loadGlyphMap,
			   do_setKeyboards,
			   do_setDefaultKB,
			   do_Terminate
};
			   
			   
POA_Akka__vepv poa_akka_vepv = { &base_epv, &akka_epv };

POA_Akka poa_akka_servant = { NULL, &poa_akka_vepv };


/* dirty but easy */
int argc;
char **argv;

/** CORBA event loop thread.
 */
void corba_akka_thread ()
{
    PortableServer_ObjectId objid = {0, sizeof("MyAkkaLib"), "MyAkkaLib"};
    PortableServer_POA poa;

    CORBA_Environment ev;
    char *retval;
    CORBA_ORB orb;
    FILE * ofp;

    signal(SIGINT, exit);
    signal(SIGTERM, exit);

    CORBA_exception_init(&ev);
    orb = CORBA_ORB_init(&argc, argv, "orbit-local-orb", &ev);

    POA_Akka__init(&poa_akka_servant, &ev);

    poa = (PortableServer_POA)CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);
    PortableServer_POAManager_activate(PortableServer_POA__get_the_POAManager(poa, &ev), &ev);
    PortableServer_POA_activate_object_with_id(poa,
                                               &objid, &poa_akka_servant, &ev);

    akka_client = PortableServer_POA_servant_to_reference(poa,
                                                          &poa_akka_servant,
                                                          &ev);
    if (!akka_client)
        fprintf(stderr, "Cannot get objref\n");


    retval = CORBA_ORB_object_to_string(orb, akka_client, &ev);

    ofp = fopen("/tmp/.akka.ior","w");
    if( ofp == NULL ) {
      perror("Could not open .akka.ior for writing");
      exit(EXIT_FAILURE);
    }

    fprintf(ofp,"%s", retval);
    fclose(ofp);

    CORBA_free(retval);
    CORBA_ORB_run(orb, &ev);

    return;
}

/** start the CORBA event loop thread.
    @param ac Must be the same as main's argc.
    @param av Must be the same as main's argv.
*/

void start_corba_akka_server(int ac, char **av) {
  pthread_t nacs;

  argc=ac;
  argv=av;

  if(pthread_create(&nacs, NULL, (void*)&corba_akka_thread, NULL)!=0){
    perror("Could not create Akka server thread");
    exit(EXIT_FAILURE);
  }
  return;
}
