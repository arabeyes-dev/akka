%module akka_perl_mod


extern void set_direction(int, int);
extern void set_bidiapprox(int, int, int);
extern void remove_console(int);
extern void add_console(int);
extern void process_glyph(int, int);
extern void load_glyph_map(int,int,int,int,int,int);
extern void set_keymap(int, char *, char *);
extern void set_default_keymap(char *);
extern void terminate_server();