########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
########################################

use strict;

my @LIBDIRS;

#TODO : add analysis of versions
sub locatelib {
  my $arg = shift @_;
  my $dir;
  my $file;
  my $lib;
  my $param;
  print "Looking for $arg library: ";
  $param = '\w*' . $arg . '\w*\.so\w*';
  foreach $dir (@LIBDIRS) {
    if (opendir(DIRC, $dir)) {
      my @dirc = readdir(DIRC);
      foreach $file (@dirc) {
	if ($file =~ /$param/) {
	  $lib = substr($file,3);
	  $lib =~ s/\.so.*//;
	  print "lib$lib found in $dir\n";
	  closedir(DIRC);
	  return " -l$lib -L$dir";
	}
      }
      closedir(DIRC);
    }
  }
  print "not found\n";
  
}

#####################################
#   Initialization of this module   #
#####################################

if (open(LDCONF, "/etc/ld.so.conf")) {
  @LIBDIRS = <LDCONF>;
  my $dir;
  foreach $dir (@LIBDIRS) {
    chomp $dir;
  };
  close(LDCONF);
}
@LIBDIRS = ("/lib", "/usr/lib", @LIBDIRS);
