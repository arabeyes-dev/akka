########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
#                                      #
########################################

use strict;
require "macros/libraries.pl";

sub pthreadconfig {
  $Main::cache{'pthreadconfig()'}{libs}=locatelib('pthread');
}

return 1;
