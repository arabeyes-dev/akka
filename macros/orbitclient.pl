########################################
# Perl extensible replacement to the   #
# autotools                            #
# (C)2001 Chahine M. Hamila            #
# This code is provided under the      #
# GPL license which you can find at    #
# http://www.gnu.org/copyleft/gpl.html #
#                                      #
########################################

use strict;

#find the orbit-config prog
sub orbitclient {
  print "Looking for orbit-config (client config): ";
  my $orbitprog = `which orbit-config`;
  my $ldf;
  my $cfl;
  if ($orbitprog) {
    chomp $orbitprog;
    print "$orbitprog\n";
    $ldf = `$orbitprog --libs client`;
    chomp $ldf;
    $cfl = `$orbitprog --cflags client`;
    chomp $cfl;
  } else {
    print "Not found\n";
    exit;
  };
  $Main::cache{'orbitclient()'}{libs}=$ldf;
  $Main::cache{'orbitclient()'}{cflags}=$cfl;
}

return 1;
