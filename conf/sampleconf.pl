use Akka;

# default keyboard is French iBook
Akka::set_default_keymap("/etc/console-tools/default.kmap");

###############################
# Initialization of console 2 #
###############################

# monitor console number 2
Akka::add_console(2);

# console 2 will be French/Arabic Akka
Akka::set_keymap(2, "/home/hanibaal/progs/arabeyes/projects/akka/keymaps/pc/fr-latin0.map",
		  "/home/hanibaal/progs/arabeyes/projects/akka/keymaps/pc/arabic.map");

# console 2 will not render glyphs, which is the default
# we will load the sample "square arabic" font
`consolechars --tty=/dev/tty2 -m straight-to-font -f /home/hanibaal/progs/arabeyes/projects/akka/fonts/iso06b.fnt`;

# We want console 2 to start as an RTL console
Akka::set_direction(2, 1);

###############################
# Initialization of console 3 #
###############################

# monitor console number 3
Akka::add_console(3);

# console 3 will be French/Arabic Akka iBook as well (could be English/Urdu, or Spanish/Syriac for example...)
Akka::set_keymap(3, "/home/hanibaal/progs/arabeyes/projects/akka/keymaps/pc/fr-latin0.map",
		  "/home/hanibaal/progs/arabeyes/projects/akka/keymaps/pc/arabic.map");

# We want glyph rendering on console 3
# We have to inform Akka in order to have it make the necessary initializations
Akka::process_glyph(3, 1);

# We load the necessary filter now
Akka::load_glyph_file(3, "/home/hanibaal/progs/arabeyes/projects/akka/glyph/iso06glyph.map");

# Loading the appropriate font
`consolechars --tty=/dev/tty3 -m straight-to-font -f /home/hanibaal/progs/arabeyes/projects/akka/fonts/sample.f16`;

###############################
# Initialization of console 4 #
###############################

# monitor console number 4
Akka::add_console(4);

# console 4 will be French/Arabic Akka iBook...
Akka::set_keymap(4, "/home/hanibaal/progs/arabeyes/projects/akka/keymaps/pc/fr-latin0.map",
		  "/home/hanibaal/progs/arabeyes/projects/akka/keymaps/pc/arabic.map");

# We want this console to approximate bidi with a character encoding set to ISO-8859-6
Akka::set_bidi_approximation(4,1,$Akka::ISO06);


# We want console 4 to start as an RTL console
Akka::set_direction(4, 1);

# We want glyph rendering on console 4
# We have to inform Akka in order to have it make the necessary initializations
Akka::process_glyph(4, 1);

# We load the necessary filter now
Akka::load_glyph_file(4, "/home/hanibaal/progs/arabeyes/projects/akka/glyph/iso06glyph.map");

# Loading the appropriate font
`consolechars --tty=/dev/tty4 -m straight-to-font -f /home/hanibaal/progs/arabeyes/projects/akka/fonts/sample.f16`;
