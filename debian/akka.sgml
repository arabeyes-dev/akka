<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Mohammed</firstname>">
  <!ENTITY dhsurname   "<surname>Elzubeir</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>March 25, 2002</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>8</manvolnum>">
  <!ENTITY dhemail     "<email>elzubeir@arabeyes.org</email>">
  <!ENTITY dhusername  "Mohammed Elzubeir">
  <!ENTITY dhucpackage "<refentrytitle>AKKA</refentrytitle>">
  <!ENTITY dhpackage   "akka">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2001</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>
      A tool for Arabizing *n*x consoles.
    </refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>&nbsp;allows you to read and write in Arabic under the 
      text console using any existing software without any change.
     </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <command>&dhpackage;</command>
    </para>

    <para>Once you run <command>&dhpackage;</command> it will be loaded to memory
    and run in daemon mode. You will then need to run 
    <command>akka-conf.pl</command>, located in /etc/akka. This script will
     configure your terminals to the following by default:</para>

    <variablelist>
      <varlistentry>
        <term>Terminal 1</term>
        <listitem>
        <para>Not affected</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Terminal 2</term>
        <listitem>
        <para>Arabic/English (Squared Arabic)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Terminal 3</term>
        <listitem>
        <para>Arabic/English (Shaped Arabic)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Terminal 4</term>
        <listitem>
        <para>Arabic/English (Shaped Arabic) plus Bidi approximation</para>
        </listitem>
      </varlistentry>
    </variablelist>

  <para>Once you are switched to one of the different terminals (using the
        alt-F? keys), there are several options you have:</para>
 
    <variablelist>
      <varlistentry>
        <term>F10</term>
        <listitem>
        Insert mode (LTR, RTL -- cursor doesn't move, pushing the characters out)
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>F11</term>
        <listitem>
        Alternate between languages (e.g. Arabic/English)
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>F12</term>
        <listitem>
        Mirror screen (make everything Left-To-Right or Right-To-Left)
        </listitem>
      </varlistentry>
    </variablelist>


  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <para>No options available.</para>

  </refsect1>
  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &dhemail; for
      the &debian; system (but may be used by others).  Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the <acronym>GNU</acronym> Free Documentation
      License, Version 1.1 or any later version published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts and no Back-Cover Texts.</para>

  </refsect1>

  <refsect1>
    <title>BUG REPORTS</title>
    <para>Please submit all bug reports to the Arabugs on
          http://www.arabeyes.org/arabugs</para>
  </refsect1>

</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
